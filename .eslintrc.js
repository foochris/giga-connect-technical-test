module.exports = {
	root: true,
	env: {
		node: true,
	},
	extends: [
		'plugin:vue/essential',
		'eslint:recommended',
		'@vue/airbnb',
	],
	rules: {
		'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

		// tabs not spaces
		'indent': ['error', 'tab', { SwitchCase: 1 }],
		'no-tabs': 'off',
		'vue/html-indent': ['error', 'tab'],
		// max line length 240
		'max-len': ['error', { code: 240 }],
		// allow curly brackets around arrow function bodies
		'arrow-body-style': 'off',
		// allow no parens for single-argument arrow functions
		'arrow-parens': ['error','as-needed'],
		// allow no return statement
		'consistent-return': 'off',
		// allow (but do not require) dangling commas
		'comma-dangle': ['error', 'only-multiline'],
		// allow promises to be rejected with non-errors
		'prefer-promise-reject-errors': 'off',
		// allow multiple attributes per line in vue templates
		'vue/max-attributes-per-line': 'off',
		// disallow non-void html elements to be self-closing
		'vue/html-self-closing': ['error', {
			html: {
				normal: 'never'
			}
		}],
		'semi': ["error", "never"],
		'no-use-before-define': 0,
		'no-underscore-dangle':  ['error', { 'allow': ['_popup', '_topClusterLevel'] }],
		'import/no-extraneous-dependencies': ['error', {'devDependencies': true}],
	},
	parserOptions: {
		parser: 'babel-eslint',
	},
}
