const GoogleFontsPlugin = require('@beyonk/google-fonts-webpack-plugin')

module.exports = {
	// eslint-disable-next-line
	publicPath: ['production', 'staging'].includes(process.env.VUE_APP_MODE) && process.env.hasOwnProperty('CI_PROJECT_NAME')
		? `/${process.env.CI_PROJECT_NAME}/`
		: '/',

	css: {
		extract: false
	},

	chainWebpack: config => {
		config
			.plugin('html')
			.tap(args => {
				// eslint-disable-next-line
				args[0].template = './src/assets/index.html'

				return args
			})
	},

	configureWebpack: {
		plugins: [
			new GoogleFontsPlugin({
				fonts: [
					{
						family: 'Lato',
						variants: [
							'100',
							'100italic',
							'300',
							'300italic',
							'400',
							'400italic',
							'700',
							'700bold',
							'700bolditalic',
							'700italic',
							'900',
							'900italic',
							'900bolditalic',
						]
					},
				]
			})
		]
	}
}
