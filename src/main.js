import Vue from 'vue'
import Buefy from 'buefy'
import VueScrollingTable from 'vue-scrolling-table'
import Faker from 'vue-faker'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
	faCheckSquare,
	faBan,
	faQuestionCircle,
	faGlobe,
	faInfoCircle,
	faFileCsv,
	faFileExcel,
	faFilePdf,
	faCompress,
	faExpand,
} from '@fortawesome/free-solid-svg-icons'
import { faClipboard, faSquare } from '@fortawesome/free-regular-svg-icons'
import { Icon } from 'leaflet'
import VTooltip from 'v-tooltip'
import CountryFlag from 'vue-country-flag'
import App from './App.vue'

Vue.config.productionTip = false

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('vue-scrolling-table', VueScrollingTable)
Vue.component('country-flag', CountryFlag)

// icons for map (leaflet)
// eslint-disable-next-line
delete Icon.Default.prototype._getIconUrl
Icon.Default.mergeOptions({
	iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'), // eslint-disable-line
	iconUrl: require('leaflet/dist/images/marker-icon.png'), // eslint-disable-line
	shadowUrl: require('leaflet/dist/images/marker-shadow.png'), // eslint-disable-line
})

library.add(
	faCheckSquare,
	faBan,
	faQuestionCircle,
	faGlobe,
	faInfoCircle,
	faClipboard,
	faSquare,
	faFileCsv,
	faFileExcel,
	faFilePdf,
	faCompress,
	faExpand,
)

Vue.use(VTooltip)
Vue.use(Faker)

Vue.use(Buefy, {
	defaultIconPack: 'fas',
	defaultIconComponent: 'font-awesome-icon',
	defaultFieldLabelPosition: 'on-border',
})

new Vue({
	render: h => h(App),
}).$mount('#app')
