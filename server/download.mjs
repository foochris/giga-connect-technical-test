import filesize from 'filesize'
import fetch from 'node-fetch'
import fs from 'fs'
import querystring from 'querystring'
import getCountryISO2 from 'country-iso-3-to-2'

import perf from 'execution-time'

(async () => {
	const formTypes = [
		{ id: 5496, type: 'bs' }, // health facility observation
		{ id: 22540, type: 'ec' },
	]

	const context = Object.create(null)
	context.hcf = Object.create(null)
	context.rows = {}	// rows of hcf data

	context.url = 'https://im.unhcr.org/apps/ph-bsc-dashboard/php/kobo-form-filter.php'
	perf().start()

	forEachPromise(formTypes, writeFileFormType, context).then(() => {
		// remove hcf ID from object key
		const hcf = []
		Object.keys(context.hcf).forEach(key => hcf.push(context.hcf[key]))

		// merge balanced scorecard / energy checklist by using health facility ID as common identifier
		let result = [...context.rows.bs.concat(context.rows.ec).concat(hcf)
			.reduce((r, o) => {
				// eslint-disable-next-line
				r.has(o.hcf_id) || r.set(o.hcf_id, {})

				const item = r.get(o.hcf_id)

				Object.entries(o).forEach(([k, v]) => {
					item[k] = Array.isArray(item[k]) ? [...new Set([...item[k], ...v])] : v
				})

				return r
			}, new Map()).values()]

		// remove null / empty hcf values
		result = result.filter(row => row.hcf_id !== null && row.hcf_id !== '' && row.hcf_id !== undefined)

		// sort by country iso, camp name and hcf name
		result.sort((a, b) => a.country_name.localeCompare(b.country_name) || a.camp_name.localeCompare(b.camp_name) || a.hcf_name.localeCompare(b.hcf_name))

		// get all iso : to be used for lat/lon if HCF not available
		const campISOs = [...new Set(result.map(row => row.country_iso))]

		// url/params: health facilities geospatial data
		const baseUrl = 'https://gis.unhcr.org/arcgis/rest/services/sector/ph_bsc_view/MapServer/0/query?'
		const params = {
			where: '1=1',
			outFields: 'hf_code,site_code,lat,lon',
			f: 'json',
		}

		// url/params: camp sites (country ISOs)
		const siteUrl = 'https://gis.unhcr.org/arcgis/rest/services/core/core_prod/MapServer/0/query?'
		const siteParams = {
			where: `iso3 in ('${campISOs.join('\',\'')}')`, //  110
			outFields: 'pcode,iso3',
			outSr: '4326',
			f: 'json',
		}

		const urls = [
			fetch(`${baseUrl}${querystring.stringify(params)}`)
				.then(res => (res.ok && res.json()) || Promise.reject(res)),
			fetch(`${siteUrl}${querystring.stringify(siteParams)}`)
				.then(res => (res.ok && res.json()) || Promise.reject(res))
		]

		Promise.all(urls).then(([res, sites]) => {
			// prefill result lat/lon values with site coordinates (HCF data lat/lon may be null)
			result.forEach(resultRow => {
				sites.features.forEach(row => {
					// update context row if hf codes match
					if (row.attributes.pcode.trim() === resultRow.camp_id) {
						resultRow.lat = row.geometry.y // eslint-disable-line
						resultRow.lon = row.geometry.x // eslint-disable-line
					}
				})
			})

			// iterate over result rows
			result.forEach(resultRow => {
				// iterate over HCF data
				res.features.forEach(row => {
					// update context row if hf codes match and exclude lat/lon null values
					if (row.attributes.hf_code.trim() === resultRow.hcf_id.trim()
						&& row.attributes.lat !== null
						&& row.attributes.lon !== null) {
						resultRow.lat = row.attributes.lat // eslint-disable-line
						resultRow.lon = row.attributes.lon // eslint-disable-line
					}
				})
			})

			// todo: null in hcf, not listed (wrl_ppl_poc_p_unhcr), "NGAp004560"

			// write out context and result files
			let time = Number.parseFloat(perf().stop().time / 1000).toPrecision(3)
			fs.writeFileSync('./public/result.json', JSON.stringify(result))
			let size = filesize(fs.statSync('./public/result.json').size)
			console.log(`SUCCESS: result processed | ${time} s | ${size}`)

			time = Number.parseFloat(perf().stop().time / 1000).toPrecision(3)
			fs.writeFileSync('./public/context.json', JSON.stringify(context))
			size = filesize(fs.statSync('./public/context.json').size)
			console.log(`SUCCESS: context processed | ${time} s | ${size}`)
		}).catch(err => {
			console.log(err)
		})
	})
})()

/**
 *
 * @param items An array of items.
 * @param fn A function that accepts an item from the array and returns a promise.
 * @returns {Promise}
 */
function forEachPromise(items, fn, context) {
	return items.reduce((promise, item) => {
		return promise.then(() => {
			return fn(item, context)
		})
	}, Promise.resolve())
}

function writeFileFormType(formType, context) {
	return new Promise(resolve => {
		process.nextTick(() => {
			perf().start()

			fetch(`${context.url}?form_type=${formType.id}`)
				.then(res => res.json())
				.then(json => {
					const data = Object.create(null)

					json.forEach(row => {
						if ('gstart/xID_final' in row && row['gstart/xID_final'] in data) {
							const surveyDate = new Date(data[row['gstart/xID_final']].survey_date).getTime() / 1000
							const rowSurveyDate = new Date(row.surveyDate).getTime() / 1000
							const rowSubmissionTime = new Date(row._submission_time).getTime() / 1000 // eslint-disable-line

							// get most recent survey row
							if ((rowSurveyDate > surveyDate)
								|| (surveyDate === rowSurveyDate && rowSubmissionTime > surveyDate)) {
								data[row['gstart/xID_final']] = getFilteredRow(formType.type, row)
							}
						} else {
							// set initial row value
							data[row['gstart/xID_final']] = getFilteredRow(formType.type, row)
						}

						// add to hcf array
						// eslint-disable-next-line
						context.hcf[row['gstart/xID_final']] = {
							hcf_id: row['gstart/xID_final'],
							hcf_name: row['gstart/xhf_final'],
							camp_id: row['gstart/xpcode_final'],
							camp_name: row['gstart/xsite_final'],
							country_name: row['gstart/xcountry_final'],
							country_iso: row['gstart/xiso_final'],
							country_iso2: row['gstart/xiso_final'] ? getCountryISO2(row['gstart/xiso_final'].toUpperCase()) : '',
							health_services: row['gstart/gcontext6/healthService'],
							health_facility_type: row['gstart/gcontext5/HFType'],
							hospital_types: row['gstart/gcontext6/HospitalType'],
							number_observation_beds: row['gFaciInfra1/nObsBed'],
						}
					})

					// remove key from data array
					const dataNoKey = []
					Object.keys(data).forEach(key => dataNoKey.push(data[key]))

					// eslint-disable-next-line
					context.rows[formType.type] = dataNoKey

					// promise
					resolve()
				})
		})
	})
}

function getFilteredRow(formType, row) {
	// clone object
	const data = JSON.parse(JSON.stringify(row))

	const fields = Object.create(null)
	fields.bs = {
		'gstart/xID_final': 'hcf_id',
		'gFaciInfra1/watAvail': 'bs_water_available',
		'gFaciInfra1/handwashAvailTod': 'bs_handwash',
		'gFaciInfra1/sanitationAvail': 'bs_sanitation',
		'gFaciInfra2/showers': 'bs_showers',
		'gFaciInfra1/toiletAvail': 'bs_toilet_available',
		'gFaciInfra2/genderLat': 'bs_gender_separated',
		'gFaciInfra2/childLat': 'bs_child_friendly',
		'gFaciInfra2/lattrinesTyp': 'bs_latrine_types',
		surveyDate: 'bs_survey_date',
	}

	fields.ec = {
		'gstart/xID_final': 'hcf_id',
		'water_group/water_improved_group/water_improved': 'ec_water_improved',
		'water_group/water_available_group/water_available': 'ec_water_available',
		'water_group/water_storage_group/water_storage': 'ec_water_storage',
		'water_group/water_accessible_group/water_accessible': 'ec_water_accessible',

		'sanitation_group/toilets_outpatient_group/toilets_outpatient': 'ec_toilets_outpatient',
		'sanitation_group/toilets_inpatient_group/toilets_inpatient': 'ec_toilets_inpatient',
		'sanitation_group/toilets_mh_group/toilets_mh': 'ec_toilets_mh',
		'sanitation_group/toilets_mobility_group/toilets_mobility': 'ec_toilets_mobility',

		'hygiene_group/hygiene_poc_group/hygiene_poc': 'ec_hygiene_poc',
		'hygiene_group/hygiene_toilet_group/hygiene_toilet': 'ec_hygiene_toilet',

		'waste_group/waste_segregation_group/waste_segregation': 'ec_hygiene_poc',
		'waste_group/waste_infectious_group/waste_infectious': 'ec_waste_infectious',
		'waste_group/waste_sharp_group/waste_sharp': 'ec_waste_sharp',

		'environmental_group/cleaning_protocols_group/cleaning_protocols': 'ec_cleaning_protocols',
		'environmental_group/cleaning_training_group/cleaning_training': 'ec_cleaning_training',

		'energy_group/electricity_source': 'ec_electricity_source',
		'energy_group/electricity_medical_devices': 'ec_electricity_medical_devices',
		'energy_group/electricity_stand_alone': 'ec_electricity_stand_alone',
		'energy_group/electricity_use': 'ec_electricity_use',
		'energy_group/electricity_secondary': 'ec_electricity_secondary',
		'energy_group/electricity_generator': 'ec_electricity_generator',
		'energy_group/electricity_reliable': 'ec_electricity_reliable',

		surveyDate: 'ec_survey_date',
	}

	// remove unrequired keys/fields
	Object.keys(data).forEach(key => {
		if (!Object.keys(fields[formType]).includes(key)) {
			delete data[key]
		}
	})

	// rename keys
	Object.keys(fields[formType]).forEach(key => {
		if (!(key in data)) {
			data[fields[formType][key]] = null
		} else {
			data[fields[formType][key]] = data[key]
			delete data[key]
		}
	})

	return data
}
